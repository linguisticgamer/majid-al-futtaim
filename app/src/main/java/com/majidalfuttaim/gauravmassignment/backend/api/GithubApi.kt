package com.majidalfuttaim.gauravmassignment.backend.api

import com.majidalfuttaim.gauravmassignment.backend.model.Repo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

//https://api.github.com/users/ethanboxx/repos
interface GithubApi {

    @GET("users/{user}/repos")
    suspend fun searchUserOnId(
        @Path("user") userName:String
    ): Response<List<Repo>>


}