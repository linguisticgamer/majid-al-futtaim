package com.majidalfuttaim.gauravmassignment.ui.viewmodels

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.majidalfuttaim.gauravmassignment.backend.model.Repo
import com.majidalfuttaim.gauravmassignment.backend.repository.ProfileSearchRepo
import com.majidalfuttaim.gauravmassignment.backend.retrofit.Result
import com.majidalfuttaim.gauravmassignment.backend.retrofit.resultLiveData
import com.majidalfuttaim.gauravmassignment.databinding.ItemSearchRowBinding
import com.majidalfuttaim.gauravmassignment.ui.adapters.GenericRecyclerAdapter

class SearchViewModel(var mApplication: Application) :AndroidViewModel(mApplication) {

    var searchList:ArrayList<Repo> = ArrayList()
    var searchName:String = ""
    var searchAdapter: GenericRecyclerAdapter<Repo, ItemSearchRowBinding>? = null

    fun changeSearchName(search:String){
        searchName = search.trim()
        clearSearchList()
        searchRepo()
    }

    fun addToList(searchArray:List<Repo>){

        searchList.addAll(searchArray)
        notifyAdapterChanged()
    }

    fun clearSearchList(){
        searchList.clear()
    }

    fun notifyAdapterChanged(){
        searchAdapter!!.notifyDataSetChanged()
    }

    fun searchRepo():LiveData<Result<List<Repo>>> {
        return resultLiveData { ProfileSearchRepo.searchUserOnId(searchName) }
    }

    fun share(repo:Repo){
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Hey Check out this awesome repo. \n ${repo.url}")
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        mApplication.startActivity(shareIntent)
    }

}