package com.majidalfuttaim.gauravmassignment.ui.interfaces

interface GenericRecyclerInterface<LVM, T>{
    fun bindData(binder: LVM, model: T, pos: Int)
}