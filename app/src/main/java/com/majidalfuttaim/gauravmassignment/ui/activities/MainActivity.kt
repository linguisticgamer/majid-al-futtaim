package com.majidalfuttaim.gauravmassignment.ui.activities

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.majidalfuttaim.gauravmassignment.R
import com.majidalfuttaim.gauravmassignment.backend.model.Repo
import com.majidalfuttaim.gauravmassignment.backend.retrofit.Result
import com.majidalfuttaim.gauravmassignment.databinding.ActivityMainBinding
import com.majidalfuttaim.gauravmassignment.databinding.ItemSearchRowBinding
import com.majidalfuttaim.gauravmassignment.ui.adapters.GenericRecyclerAdapter
import com.majidalfuttaim.gauravmassignment.ui.interfaces.GenericRecyclerInterface
import com.majidalfuttaim.gauravmassignment.ui.viewmodels.SearchViewModel
import okhttp3.internal.notify


class MainActivity : BaseActivity<ActivityMainBinding>(){

    lateinit var mActivityMainBinding: ActivityMainBinding
    lateinit var mSearchViewModel:SearchViewModel
    private var delay: Long = 1000
    private var searchLastEditTextTime: Long = 0
    private var handler = Handler()



    override fun getLayoutResourceId(): Int {
        return R.layout.activity_main;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityMainBinding = mActivityBinding
        initUI()
    }

    private fun initUI(){
        mSearchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        initRecyclerView()
        observeSearchKeyword()
    }


    private fun initRecyclerView(){
        if(mSearchViewModel.searchAdapter == null) {
            mSearchViewModel.searchAdapter =
                GenericRecyclerAdapter(mSearchViewModel.searchList, R.layout.item_search_row, genericRecyclerInterface)

        }

        if(mActivityMainBinding.rvSearch.adapter == null) {
            mActivityMainBinding.rvSearch.adapter = mSearchViewModel.searchAdapter
        }
    }

    private fun observerSearchList() {
        if(mSearchViewModel.searchName != "") {
            mSearchViewModel.searchRepo().observe(this, Observer {
                if (it.status != Result.Status.LOADING) {
                    if (!it.data.isNullOrEmpty()) {
                        mActivityMainBinding.tvErrorText.visibility = View.INVISIBLE
                        mSearchViewModel.addToList(it.data)
                    } else {
                        mActivityMainBinding.tvErrorText.visibility = View.VISIBLE
                        mSearchViewModel.clearSearchList()
                        mSearchViewModel.notifyAdapterChanged()
                        showToast("No repo found for user.")
                    }
                } else if (it.status == Result.Status.ERROR) {
                    showToast("Something went wrong, Please try again later")
                }
            })
        }
        else{
            mActivityMainBinding.tvErrorText.visibility = View.VISIBLE
            mSearchViewModel.clearSearchList()
            mSearchViewModel.notifyAdapterChanged()
        }
    }


    /* Just to make sure we do not search after every word, right now I am taking 1.5 second delay.*/

    private fun observeSearchKeyword(){
        mActivityMainBinding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchLastEditTextTime = System.currentTimeMillis()
                handler.postDelayed(inputFinishChecker, delay)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                handler.removeCallbacks(inputFinishChecker)
            }

        })
    }

    private val inputFinishChecker = Runnable {
        if (System.currentTimeMillis() > searchLastEditTextTime + delay) {
                mSearchViewModel.changeSearchName(mActivityMainBinding.etSearch.text.toString())
                observerSearchList()
        }
    }

    private val genericRecyclerInterface = object:
        GenericRecyclerInterface<ItemSearchRowBinding, Repo> {
        override fun bindData(binder: ItemSearchRowBinding, model: Repo, pos: Int) {
            binder.repo = model
            if(model.description.isNullOrEmpty()){
                binder.tvSearchDescription.visibility = View.GONE
            }
            binder.tvShare.setOnClickListener {
                mSearchViewModel.share(model)
            }
        }

    }


}